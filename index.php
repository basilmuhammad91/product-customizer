<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create Custom Product Design</title>

    <!-- include bootstrap css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- include bootstrap js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- include custom scss file -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Jquery cdn -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>

<body>
    <section>
        <div class="container">
            <div class="row mt-5 pt-5">
                <div class="col-md-4 make-style-form">
                    <ul class="nav nav-pills mb-3 custom_tab" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">TEXT</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">FONT</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">COLOR</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row">
                                <div class="col-12">
                                    <textarea name="" id="input_text" cols="50" rows="3" placeholder="ENTER TEXT HERE 
Press Enter/Return for a new line"></textarea>
                                </div>
                                <div class="col-12">
                                    <div class="forn-group size_style">
                                        <label for="">Select your options</label>
                                    </div>
                                </div>
                                <div class="col-12 px-3">
                                    <div class="row">
                                        <div class="col-6 px-1 mb-2 main_box">
                                            <div class="size_box row mx-0 px-0" onclick="measure(this)">
                                                <div class="col-5">
                                                    <strong style="height: 24px;">Small</strong>
                                                    <h5 class="mt-1">$268</h5>
                                                </div>
                                                <div class="col-7 size_sec_right">
                                                    <strong>Length: <span id="length">55</span>"</strong>
                                                    <strong>Height: <span id="height">25</span>"</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 px-1 mb-2 main_box">
                                            <div class="size_box row mx-0 px-0" onclick="measure(this)">
                                                <div class="col-5">
                                                    <strong style="height: 24px;">Small</strong>
                                                    <h5 class="mt-1">$268</h5>
                                                </div>
                                                <div class="col-7 size_sec_right">
                                                    <strong>Length: <span id="length">55</span>"</strong>
                                                    <strong>Height: <span id="height">25</span>"</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 px-1 mb-2 main_box">
                                            <div class="size_box row mx-0 px-0" onclick="measure(this)">
                                                <div class="col-5">
                                                    <strong style="height: 24px;">Small</strong>
                                                    <h5 class="mt-1">$268</h5>
                                                </div>
                                                <div class="col-7 size_sec_right">
                                                    <strong>Length: <span id="length">55</span>"</strong>
                                                    <strong>Height: <span id="height">25</span>"</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 px-1 mb-2 main_box">
                                            <div class="size_box row mx-0 px-0" onclick="measure(this)">
                                                <div class="col-5">
                                                    <strong style="height: 24px;">Small</strong>
                                                    <h5 class="mt-1">$268</h5>
                                                </div>
                                                <div class="col-7 size_sec_right">
                                                    <strong>Length: <span id="length">55</span>"</strong>
                                                    <strong>Height: <span id="height">25</span>"</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 px-1 mb-2 main_box">
                                            <div class="size_box row mx-0 px-0" onclick="measure(this)">
                                                <div class="col-5">
                                                    <strong style="height: 24px;">Small</strong>
                                                    <h5 class="mt-1">$268</h5>
                                                </div>
                                                <div class="col-7 size_sec_right">
                                                    <strong>Length: <span id="length">55</span>"</strong>
                                                    <strong>Height: <span id="height">25</span>"</strong>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 px-1 mb-2 main_box">
                                            <div class="size_box row mx-0 px-0" onclick="measure(this)">
                                                <div class="col-5">
                                                    <strong style="height: 24px;">Small</strong>
                                                    <h5 class="mt-1">$268</h5>
                                                </div>
                                                <div class="col-7 size_sec_right">
                                                    <strong>Length: <span id="length">55</span>"</strong>
                                                    <strong>Height: <span id="height">25</span>"</strong>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade bg-none" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <h4>Clicked 2</h4>
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <h4>Clicked 3</h4>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">

                </div>
            </div>
        </div>
    </section>
</body>

</html>

<script>
    var length;
    var height;
    var price;
    var input_text = $('textarea#input_text').val();

    // $('#input_text').change(function() {
    //     $().text();
    // });



    function measure(val) {
        // length = $(val).find('#length').html();
        // height = $(val).find('#height').html();

        // add active class on size box
        $('.size_box').removeClass('active_style');
        $(val).addClass('active_style');
    }
</script>